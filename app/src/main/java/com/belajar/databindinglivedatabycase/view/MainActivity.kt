package com.belajar.databindinglivedatabycase.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.belajar.databindinglivedatabycase.R
import androidx.databinding.DataBindingUtil
import com.belajar.databindinglivedatabycase.databinding.ActivityMainBinding
import com.belajar.databindinglivedatabycase.viewmodel.UserViewModel

class MainActivity : AppCompatActivity() {

    lateinit var binding:ActivityMainBinding
    lateinit var userViewModel: UserViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      binding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        userViewModel = UserViewModel()
        binding.viewModel = userViewModel
        binding.lifecycleOwner = this

    }
}