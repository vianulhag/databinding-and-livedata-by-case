package com.belajar.databindinglivedatabycase.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.belajar.databindinglivedatabycase.model.UserModel

class UserViewModel : ViewModel() {
    var userModel:MutableLiveData<UserModel> = MutableLiveData()
    init {
        userModel.value = UserModel()
    }
}