package com.belajar.databindinglivedatabycase.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.belajar.databindinglivedatabycase.BR

class UserModel : BaseObservable() {
    @get:Bindable
    var nama:String = ""
    set(value) {
        field = value
        notifyPropertyChanged(BR.nama)
    }

}